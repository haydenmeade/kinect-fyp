################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../axisCords.cpp \
../keyFrame.cpp \
../keyFrameVector.cpp \
../main.cpp \
../timer.cpp 

CU_SRCS += \
../mmult.cu 

CU_DEPS += \
./mmult.d 

OBJS += \
./axisCords.o \
./keyFrame.o \
./keyFrameVector.o \
./main.o \
./mmult.o \
./timer.o 

CPP_DEPS += \
./axisCords.d \
./keyFrame.d \
./keyFrameVector.d \
./main.d \
./timer.d 


# Each subdirectory must supply rules for building sources it contributes
%.o: ../%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: NVCC Compiler'
	/usr/local/cuda-7.5/bin/nvcc -I/usr/local/include -I/usr/local/cuda/include -O3 -gencode arch=compute_52,code=sm_52  -odir "." -M -o "$(@:%.o=%.d)" "$<"
	/usr/local/cuda-7.5/bin/nvcc -I/usr/local/include -I/usr/local/cuda/include -O3 --compile  -x c++ -o  "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

%.o: ../%.cu
	@echo 'Building file: $<'
	@echo 'Invoking: NVCC Compiler'
	/usr/local/cuda-7.5/bin/nvcc -I/usr/local/include -I/usr/local/cuda/include -O3 -gencode arch=compute_52,code=sm_52  -odir "." -M -o "$(@:%.o=%.d)" "$<"
	/usr/local/cuda-7.5/bin/nvcc -I/usr/local/include -I/usr/local/cuda/include -O3 --compile --relocatable-device-code=false -gencode arch=compute_52,code=compute_52 -gencode arch=compute_52,code=sm_52  -x cu -o  "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


