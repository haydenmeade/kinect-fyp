/*
 * keyFrameVector.cpp
 *
 *  Created on: 15 Sep 2016
 *      Author: hayden
 */

#include "keyFrameVector.h"
#include "timer.h"
#include "kdtree.h"
#include <TooN/GR_SVD.h>
#include <TooN/se3.h>
#include <TooN/SymEigen.h>

extern void nearest( struct kd_node *root, struct kd_node *nd, int i, struct kd_node **best, float *best_dist,float *num);
extern int visited;
extern struct kd_node* make_tree (struct kd_node *t, int len, int i);

keyFrameVector::keyFrameVector(int n){
	KeyFrames.clear();
	KeyFrames.reserve(n);
	currentKeyFrame = -1;
}

void keyFrameVector::reset(int n){
	KeyFrames.clear();
	KeyFrames.reserve(n);
	currentKeyFrame = -1;
}

keyFrame* keyFrameVector::search(int * num2,SE3<> *Eicp){
	// Searches for E of keyframe and live frame
	*Eicp = KeyFrames[currentKeyFrame].Ecurrent; // set first transformation

	// want to get transformation between current kf -> 0
	// then num2 -> 0
	// then add together
	// or currentkf and num2 -> same parent frame

	int temp = num2[0];
	int temp2 = KeyFrames[currentKeyFrame].num2[0];
	if(temp != currentKeyFrame){
		while(temp != 0){ // while we haven't reached the root node yet add inverse of num2->nextframe
			*Eicp *= KeyFrames[temp].KFconnections[0].E.inverse();
			temp = KeyFrames[temp].KFconnections[0].connection->num2[0];
		}

		while(temp2 != 0){ // while we haven't reached the root node yet add inverse of num2->nextframe
			*Eicp *= KeyFrames[temp2].KFconnections[0].E;
			temp2 = KeyFrames[temp2].KFconnections[0].connection->num2[0];
		}
	}

	return &KeyFrames[num2[0]];
}

void keyFrameVector::NeighbourScan(vector<float> *handCurrent, vector<float> *ICPErrors){
	// Want to scan neighbours of current key frame, and fill in relevant errors
	// not scan current key frame

	for(int i = 0; i < KeyFrames[currentKeyFrame].KFconnections.size(); i++){ // for each connection i
		int * num2 = KeyFrames[currentKeyFrame].KFconnections[i].connection->num2;
		ICPErrors->at(num2[0]) = ICP(handCurrent,num2[0]);
	}
}

bool isin(int num, vector<int> *check){
	for(int i = 0; i < check->size(); i++){
		if(num == check->at(i))
			return true;
	}
	return false;
}

void keyFrameVector::VectorSearch(vector<float> *handCurrent, vector<float> *ICPErrors, const float threshold){
	// Want to vector, excluding current key frame and neighbours until error is < threshold
	int num = KeyFrames[0].num2[0];
	vector<int> toskip;
	toskip.push_back(currentKeyFrame);
	for(int i = 0; i < KeyFrames[currentKeyFrame].KFconnections.size(); i++)
		toskip.push_back(KeyFrames[currentKeyFrame].KFconnections[i].connection->num2[0]);

	do{ // start at 0 and go up
		if(!isin(num,&toskip))
			ICPErrors->at(num) = ICP(handCurrent,num);
	}while(ICPErrors->at(num) > threshold && (++num < KeyFrames.size()));

}

// want to perform icp on handCurrent -> keyframe number(keyFramePos)
float keyFrameVector::ICP(vector<float> *handCurrent, int keyFramePos){
		int numPoints = (handCurrent->size())/3; // number of points in current keyframe

		// Point Check
		if(numPoints > MAXPOINTSICP){ // point error
			numPoints = MAXPOINTSICP;
			cerr << "MAX POINTS ICP" << endl;
		}

		// Consts
		const long int maxLoopTime = 30000; // max time to try for match in ms
		const float max_dist = 1; // max dist to nn, used in weight function
		const bool ICPDEBUG = false; // debugging and monitoring
		const float goalError = 0.002; // precision to find transformation for (error)
		const float precision = 0.0002; // if error doesn't change by this much, stop
		const int maxIterations = 20; //maximum icp iterations

		int i = 0,j = 0;
		timer timerICP, timerParts,timerLoop; // to time various parts of icp

		// Point / Jacobian variables
		TooN::Vector<3> pointCurrent = Zeros; // Current point for transformation/jacobian calc
		TooN::Vector<3> pointCurrentTransformed = Zeros; // E * pointCurrent
		TooN::Vector<3> pointClosest = Zeros; //and closest point to pointCurrent
		TooN::Vector<3> errors = Zeros; // Error for single point
		TooN::Vector<6> alpha = Zeros; // alpha for calculation of E
		float Ja,Jb,Jc; //Used in jacobian calculation
		float weight; // Used for weight matrix

		int nr_rows_J = handCurrent->size();
		int nr_cols_J = 6;
		float *h_Jacobian = (float *)malloc(nr_rows_J * nr_cols_J * sizeof(float));//Jacobian array
		float *h_Result = (float *)malloc(nr_cols_J * nr_cols_J * sizeof(float)); // result in 6x6
		float *h_Inverse = (float *)malloc(nr_cols_J * nr_cols_J * sizeof(float)); //inverse of h_result
		float *h_Errors = (float *)malloc(nr_rows_J * sizeof(float)); // errors
		float *h_alpha = (float *)malloc(nr_cols_J * sizeof(float)); // alpha
		float *h_JacobianT = (float *)malloc(nr_rows_J * nr_cols_J * sizeof(float)); //Jacobian transpose
		float *WeightVec = (float *)malloc(numPoints * sizeof(float)); // weight vec

		//Zero arrays
		zeroArray(h_Jacobian, nr_rows_J*nr_cols_J);
		zeroArray(h_Result, nr_cols_J * nr_cols_J);
		zeroArray(h_Inverse, nr_cols_J*nr_cols_J);
		zeroArray(h_Errors, nr_rows_J);
		zeroArray(h_alpha, nr_cols_J);
		zeroArray(h_JacobianT, nr_rows_J * nr_cols_J);
		zeroArray(WeightVec, numPoints);

		SE3<> Eicp;// = KeyFrames[currentKeyFrame].Ecurrent; // live frame to current keyframe being tracked
		keyFrame *ckf; // keyframe to perform icp on


		if(keyFramePos == currentKeyFrame){
			ckf = search(KeyFrames[keyFramePos].num2, &Eicp); // search for transformation from live frame to keyframepos, returns Eicp and pointer to keyframe
		}
		else{// if we are scanning neighbours// TODO, NEED TO SET ICPROOT AND EICP FOR KEYFRAMES THAT AREN'T CURRENT KF
			ckf = search(KeyFrames[keyFramePos].num2,&Eicp);
		}
		struct kd_node *icpRoot = ckf->root;

		// Initialise variables for ICP
		int iter = 0; // ICP iterations counter
		float errorSum = 999.0; // Sum of ICP error estimation
		float errorSumPrev = 9999.0;
		visited = 0; // kd nodes visited searching for nn
		int iterz = 0; // iterations inside for loop, used for timing calcs
		int ICPCount = 0; // number of times icp done
		struct kd_node *found; // found nn point
		float best_dist; // distance to nn
		float numm; // used in dk tree
		int numberRejected = 0; // number of points with dist>max_dist or normal vectors pointing away
		int numPointsSum = 0; // total number of points icp performed on
		timerParts.start();timerICP.start();timerLoop.start(); //timer start
		long int TimeMM = 0,TimeNN = 0,TimeWJ = 0,TimeT = 0,TimeMM2 = 0, TimeMMV2 = 0; //timers

		while((iter < maxIterations) &&
				(errorSum > goalError) &&
				(abs(errorSum - errorSumPrev) > precision) &&
				timerLoop.getTime() < maxLoopTime){
			++iter;
			if(iter > 1){
				timerParts.reset();
				mmult2(h_JacobianT, h_Jacobian, h_Result, nr_rows_J, nr_cols_J);

				Matrix<6> MatResult = Zeros;
				for(i = 0; i < 6; i++){ //row
					for(j = 0; j < 6; j++){//col
						MatResult(i,j) = h_Result[i + 6*j];
					}
				}

				GR_SVD<6> SVDResult(MatResult);
				Matrix<6> ID = Identity;
				MatResult = SVDResult.backsub(ID); // get inverse matrix

				for(i = 0; i < 6; i++){// copy matresult to h_Inverse
					for(j = 0; j < 6; j++){
						h_Inverse[i + 6*j] = MatResult(i,j);
					}
				}

				mmult3_2(h_Inverse, h_JacobianT, h_Errors, h_alpha, nr_cols_J, nr_rows_J);

				for(i = 0;i < 6; i++){
					alpha[i] = -1.0f * h_alpha[i]; // copy h_alpha to alpha and *-1
				}
				TimeMMV2 +=timerParts.getTime();
			}

			Eicp = SE3<>::exp(alpha) * Eicp; // new SE3 transformation update

			timerParts.reset();
			for(i = 0; i < numPoints; i++){
				iterz++;
				pointCurrent = makeVector(handCurrent->at(i*3),handCurrent->at(i*3+1),handCurrent->at(i*3+2)); // Point in handCurrent
				pointCurrentTransformed = Eicp * pointCurrent; // estimated transformed point (want it to be the point in handPrev

				// Point to search in kd tree
				struct kd_node pointCurrentkd = {{pointCurrentTransformed[0],pointCurrentTransformed[1],pointCurrentTransformed[2]}};
				found = 0;
				numm = 0;
				nearest(icpRoot, &pointCurrentkd,0,&found,&best_dist,&numm); // find nearest point
				pointClosest = makeVector(found->pt[0],found->pt[1],found->pt[2]);

				errors = pointCurrentTransformed - pointClosest;
				TimeNN += timerParts.getTime();
				timerParts.reset();
				////////////////////////////////
				//// Weighting errors
				if(max_dist < best_dist){
					cerr << "Max dist reached: " << max_dist << " " << best_dist << endl;
					weight = 0.f;
					++numberRejected;
				}
				else{
					weight = max_dist/(max_dist+best_dist);
				}

				//cerr << weight << endl;

				////////////////////
				// Calculating normal of key frame hand
				if(found->idx > 11){
					const int numPointsNormal = 3; // needs to be odd (3,5)
					TooN::Vector<3> v[sq(numPointsNormal)]; // grid of points to find normal
					for(j = 0; j < sq(numPointsNormal); j++){
						int idx1 = (found->idx) + 3*(j - (numPointsNormal+1));

						if(idx1 < ckf->hand.size())
							v[j] = makeVector(ckf->hand.at(idx1),ckf->hand.at(idx1 + 1),ckf->hand.at(idx1 + 2)); // fill v with closest points
						else{
							cerr << "ERR" << endl << found->idx << " idx1: " << idx1 << endl << j << endl;
							v[j] = makeVector(ckf->hand.at(found->idx),ckf->hand.at((found->idx) + 1),ckf->hand.at((found->idx) + 2));
						}
					}

					// now we have vector v with 9 points, want to calculate plane of best fit
					TooN::Vector<3> meanP = Zeros; // mean point
					for(j = 0; j < sq(numPointsNormal);j++){ // find mean point
						meanP += v[j];
					}
					meanP /= sq(numPointsNormal);

					Matrix<3> Cov = Zeros;
					for(j = 0; j < sq(numPointsNormal); j++){
						TooN::Vector<3> diff = v[j] - meanP;
						Cov += diff.as_col() * diff.as_row();
					}
					SymEigen<3> Eig(Cov);
					TooN::Vector<3> EigValues = Eig.get_evalues();
					TooN::Matrix<3> EigVectors = Eig.get_evectors();
					TooN::Vector<3> EVectorSmallest = EigVectors[2];
					EVectorSmallest = Eicp.inverse() * EVectorSmallest;
					if(EVectorSmallest[2] < 0){// if surface normal pointing away from camera
						weight = 0.f;
						numberRejected++;
					}
				}

				// fill WeightVec
				WeightVec[i] = weight;

				//error vec
				h_Errors[ i * 3 ] = errors[0];
				h_Errors[i*3 + 1] = errors[1];
				h_Errors[i*3 + 2] = errors[2];

				/// Calculate new Jacobian
				Ja = pointCurrent[0] - alpha[5] * pointCurrent[1] + alpha[4] * pointCurrent[2] + alpha[0];
				Jb = alpha[5] * pointCurrent[0] + pointCurrent[1] - alpha[3] * pointCurrent[2] + alpha[1];
				Jc = -alpha[4] * pointCurrent[0] + alpha[3] * pointCurrent[1] + pointCurrent[2]  + alpha[2];
				//Jacobian: (i*3) + stride(numpoints)*col
				//0 col
				h_Jacobian[i*3 + numPoints*3*0] = 1.f;
				h_Jacobian[i*3 + numPoints*3*0 + 1] = h_Jacobian[i*3 + numPoints*3*0 + 2] = 0.f;
				//1st
				h_Jacobian[i*3 + numPoints*3*1 + 1] = 1.f;
				h_Jacobian[i*3 + numPoints*3*1] = h_Jacobian[i*3 + numPoints*3*1 + 2] = 0.f;
				//2nd
				h_Jacobian[i*3 + numPoints*3*2 + 2] = 1.f;
				h_Jacobian[i*3 + numPoints*3*2 + 1] = h_Jacobian[i*3 + numPoints*3*2] = 0.f;
				//3rd
				h_Jacobian[i*3 + numPoints*3*3] = 0.f;
				h_Jacobian[i*3 + numPoints*3*3 + 1] = -Jc;
				h_Jacobian[i*3 + numPoints*3*3 + 2] = Jb;
				//4th
				h_Jacobian[i*3 + numPoints*3*4] = Jc;
				h_Jacobian[i*3 + numPoints*3*4 + 1] = 0.f;
				h_Jacobian[i*3 + numPoints*3*4 + 2] = -Ja;
				//5th
				h_Jacobian[i*3 + numPoints*3*5] = -Jb;
				h_Jacobian[i*3 + numPoints*3*5 + 1] = Ja;
				h_Jacobian[i*3 + numPoints*3*5 + 2] = 0.f;

				/// Calculate new Jacobian
				Ja = Ja*weight;
				Jb = Jb*weight;
				Jc = Jc*weight;
				//Jtranspose:((i*3 + col)*stride(6) + row
				// 0 col
				h_JacobianT[i*3*6] = weight;
				h_JacobianT[i*3*6+4] = Jc;
				h_JacobianT[i*3*6+5] = -Jb;
				//1st
				h_JacobianT[(i*3 + 1)*6 + 1] = weight;
				h_JacobianT[(i*3 + 1)*6+3] = -Jc;
				h_JacobianT[(i*3 + 1)*6+5] = Ja;
				//2nd
				h_JacobianT[(i*3 + 2)*6+2] = weight;
				h_JacobianT[(i*3 + 2)*6+4] = -Ja;
				h_JacobianT[(i*3 + 2)*6+3] = Jb;

				TimeWJ += timerParts.getTime();

			} // end for loop
			numPointsSum+=numPoints;
			//cerr<<"number rejected: " << numberRejected <<" out of " << numPoints << endl;


			errorSumPrev = errorSum;
			errorSum = 0.f;
			for(i = 0; i < numPoints; i++){ // Get weighted error sum
				 errorSum += WeightVec[i]*sqrt(sq(h_Errors[i*3]) + sq(h_Errors[i*3+1]) + sq(h_Errors[i*3+2]));
			}
			errorSum/=numPoints;

			if(errorSum == 0){
				cerr << "ERROR" << endl;
				break;
			}
			if(ICPDEBUG){
				cout << "ICP Iteration: " << iter << endl;
				cout << "n: " << numPoints << endl;
				cout << "new alpha: " << endl << alpha << endl << endl;
				cout << "SE3 Mat "<< endl<< Eicp << endl;
				cout << "error sum " << errorSum << endl << "prev error sum: "<< errorSumPrev <<endl;
				cout << "error difference: " << abs(errorSumPrev - errorSum) << endl;
			}
			TimeT += timerICP.getTime();
			timerICP.reset();
		}// end while loop
//		cerr << "Exit conditions: "<< iter << endl;
//		cerr << !(iter < maxIterations) << !(errorSum > goalError) <<  !(abs(errorSumPrev - errorSum) > precision) << !(timerLoop.getTime() < maxLoopTime)<< endl;
//		cerr << errorSum << endl;
//		cout << "ICP times: "<<endl;
// 		cout << " MMV2: " << (float)TimeMMV2/TimeT * 100 << " " << TimeMMV2 << endl;
//		cout << " NN: " << (float)TimeNN/TimeT * 100 << " " << TimeNN << endl;
//		cout << " W+J " << (float)TimeWJ/TimeT * 100 << " " << TimeWJ << endl;
//		cout << ckf->num2[0]  << endl;
		//cout << ": number rejected: " << numberRejected <<" out of " << numPointsSum << endl;
		// Mem free
		free(h_Errors);free(h_Inverse);free(h_Jacobian);free(h_Result);free(h_alpha);free(h_JacobianT);free(WeightVec);
		h_Errors = h_Inverse = h_Jacobian = h_Result = h_alpha = h_JacobianT = WeightVec = NULL;

		//return
		ckf->Ecurrent = Eicp;
		ckf->error = (float)errorSum;
		ckf = NULL;
		return (float)errorSum;
	}

