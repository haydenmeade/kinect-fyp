# README #

Kinect hand recognition and tracking

by Hayden Meade

Feel free to look at the Wiki for demo videos.

Overview:

1. Takes input depth and colour images from Kinect

2. Threshold Depth image to needed depth pixels (0.2m - 1.5m)

3. Map colour image onto depth image pixels (Registration)

4. Threshold this colour image into skin colour regions

5. Use a flood fill algorithm to segment out the hand, if present

6. Create point cloud for the hand ((x,y,z) coordinates)

7. Constructs tree like structure of key frames (frames that represent significant change in the hand pose)

8. Performs Iterative Closest Point on two point clouds, (current hand, and the current key frame being tracked) to find Euclidean transformation

9. Chooses best path to take, either performing more matching on different keyframes, based on closeness to current key frame. Or creating a new node in the key frame graph

10. Displays transformation on output by using a three dimensional axis and applying the found transformation to this axis