/*
 * KFConnection.h
 *
 *  Created on: 15 Sep 2016
 *      Author: hayden
 */

#ifndef KFCONNECTION_H_
#define KFCONNECTION_H_


#include <TooN/se3.h>
using namespace TooN;

class keyFrame;
// Connection object between keyframe it is in and keyframe (connection) with transformation E
struct keyFrameConnection{
	keyFrame *connection; // connection to keyframe n (connection->num)
	SE3<> E; // SE3 transformation from connection to this keyframe

	keyFrameConnection(){
		connection = 0;
		E =  SE3<>::exp(makeVector(0,0,0,0,0,0));
	}

	keyFrameConnection(keyFrame *connectTo, SE3<> Ein){
			connection = connectTo;
			E = Ein;
	}
};

#endif /* KFCONNECTION_H_ */
