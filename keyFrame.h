/*
 * keyFrame.h
 *
 *  Created on: 15 Sep 2016
 *      Author: hayden
 */

#ifndef KEYFRAME_H_
#define KEYFRAME_H_


#include "KFConnection.h"
#include <vector>
#include <opencv2/core.hpp>
#include <TooN/se3.h>

#define MAXPOINTSICP 50000 // max number of points that ICP will ever operate on
#define sq(in) in*in

extern int mmult3_2(float *h_A, float *h_Jt, float *h_E,float *h_R, int nr_rows_Jt, int nr_cols_Jt);
extern int mmult2(float *h_At, float *h_A, float *h_B, int nr_rows_A, int nr_cols_A);
extern void zeroArray(float * Array, int numElems);

using namespace std;
using namespace cv;
using namespace TooN;

struct kd_node;

class keyFrame{
public:
	keyFrame();
	keyFrame(int n);
	keyFrame(int n[],keyFrame &connectTo);
	void reset(int ,std::vector<float>, std::vector<std::vector<cv::Point> >); // (re)initialises a keyframe
	void constructKDTree(); // Constructs kd-tree for keyframe
	void EcurrentZero(); // to zero SE3<>

	bool exist; // if KeyFrame exists yet.
	vector<float> hand; //vector of hand points (x,y,z..)
	vector<float> axisCords; // vector of axis to draw(x,y,z..)
	vector<vector<Point> > contours;// contour of hand to draw on image
	int elemCount; //number of points in hand = hand.size()/3
	int num2[3]; // number of key frame [parent connection][connection number]
	vector<keyFrameConnection> KFconnections; //connections to adjacent keyframes
	SE3<> Ecurrent; // Frame(current) -> RKF(current)
	float error; // error between this keyframe and current keyframe when tracking
	struct kd_node *root; // Pointer to root node of k-d tree of hand points
	bool kdtree; // if kd tree exists yet
private:

};

#endif /* KEYFRAME_H_ */
