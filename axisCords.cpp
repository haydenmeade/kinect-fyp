/*
 * axisCords.cpp
 *
 *  Created on: 15 Sep 2016
 *      Author: hayden
 */

#include "axisCords.h"
#include <vector>
#include <TooN/se3.h>

using namespace TooN;
using namespace std;


axisCords::axisCords(){ // initialiser
	E = SE3<>::exp(makeVector(0,0,0,0,0,0));
	initialWorldCoordinates.reserve(12);
	currentWorldCoordinates.reserve(12);
	currentCamCoordinates.reserve(8);
	currentCentrePoint.reserve(3);
	axisData.reserve(12);
	axisLength = 0.15;
}

void axisCords::clear(){
	E = SE3<>::exp(makeVector(0,0,0,0,0,0));
	initialWorldCoordinates.clear();
	currentCamCoordinates.clear();
	currentWorldCoordinates.clear();
	axisData.clear();
	initialWorldCoordinates.reserve(12);
	currentWorldCoordinates.reserve(12);
	currentCamCoordinates.reserve(8);
	axisData.reserve(12);
}

void axisCords::fillInitialAxis(){
	for(int i = 0; i < 12;i++){
		initialWorldCoordinates.push_back(axisData[i]); // initialise axis coordinates
	}
}

void axisCords::changeAxisSize(int frameNum){
	const float changeSize = 0.002;
	initialWorldCoordinates.at(3) = axisData[3] + axisLength + changeSize * frameNum;
	initialWorldCoordinates.at(7) = axisData[7] - axisLength - changeSize * frameNum;
	initialWorldCoordinates.at(11) = axisData[11] - axisLength - changeSize * frameNum;
}

void axisCords::initialiseAxis(vector<float> *handCurrent){
	float sumx = 0,sumy = 0, sumz = 0;
	int i;
	//float length = 0.15; //length of axis

	for(i = 0; i < handCurrent->size()/3; i++){
		sumx += handCurrent->at(i*3);
		sumy += handCurrent->at(i*3+1);
		sumz += handCurrent->at(i*3+2);
	}
	sumx/=handCurrent->size()/3;
	sumy/=handCurrent->size()/3;
	sumz/=handCurrent->size()/3;

	currentCentrePoint.push_back(sumx);
	currentCentrePoint.push_back(sumy);
	currentCentrePoint.push_back(sumz);
	for(i = 0; i < 4; i++){
		axisData.push_back(sumx);
		axisData.push_back(sumy);
		axisData.push_back(sumz);
	}
	fillInitialAxis();
	changeAxisSize(0);
}

SE3<> axisCords::getEV(keyFrameVector KFV){
	// inputs vector of keyframes with number of current key frame, and sets the SE3 E
	SE3<> Etotal = KFV.KeyFrames.at(KFV.currentKeyFrame).Ecurrent;
	int tempKF = KFV.currentKeyFrame;
	while(tempKF != 0){
		Etotal *= KFV.KeyFrames.at(tempKF).KFconnections[0].E.inverse(); // add on next transformation
		tempKF = KFV.KeyFrames.at(tempKF).KFconnections[0].connection->num2[0]; // get next number in line
	}
	return Etotal;
}

void axisCords::getCurrentCords(){
	// called at end of setE // sets current cords for cam/world
	TooN::Vector<3> PointsT = Zeros;
	vector<int> cameraPointsOld(8);
	int r,c;
	int &row = r,&col = c;
	cameraPointsOld = currentCamCoordinates;
	currentCamCoordinates.clear();
	currentWorldCoordinates.clear();
	for(int i = 0; i < 4; i++){ // transform all 4 points
		PointsT[0] = initialWorldCoordinates[i*3]; //x // points in axisRoot
		PointsT[1] = initialWorldCoordinates[i*3+1];//y
		PointsT[2] = initialWorldCoordinates[i*3+2];//z
		TooN::Vector<3> tempP = E.inverse() * PointsT;
		currentWorldCoordinates.push_back(tempP[0]);
		currentWorldCoordinates.push_back(tempP[1]);
		currentWorldCoordinates.push_back(tempP[2]);
		xyztoRC(tempP[0],tempP[1],tempP[2],row,col);
		if(c > DEPTHCOLS || (c < 0 && cameraPointsOld.size() == 8)) c = cameraPointsOld[i*2+1];
		if(r > DEPTHROWS || (r < 0 && cameraPointsOld.size() == 8)) r = cameraPointsOld[i*2];
		currentCamCoordinates.push_back(r);
		currentCamCoordinates.push_back(c);
	}
}

float axisCords::getCentreErrorV(keyFrameVector KFV, vector<float> *handCurrent){
	/* Returns the error from the estimated central point of KeyFrame n, and the current hand */
	float sumx = 0,sumy = 0, sumz = 0;
	for(int i = 0; i < handCurrent->size()/3; i++){
		sumx += handCurrent->at(i*3);
		sumy += handCurrent->at(i*3+1);
		sumz += handCurrent->at(i*3+2);
	}
	sumx/=handCurrent->size()/3;
	sumy/=handCurrent->size()/3;
	sumz/=handCurrent->size()/3;

	SE3<> Ecurrent = getEV(KFV);

	TooN::Vector<3>PointsT = Zeros;
	PointsT[0] = initialWorldCoordinates[0]; //x // points in axisRoot
	PointsT[1] = initialWorldCoordinates[1];//y
	PointsT[2] = initialWorldCoordinates[2];//z
	TooN::Vector<3>CentrePoint = Ecurrent.inverse() * PointsT;
	TooN::Vector<3>CentreErrors = CentrePoint - makeVector(sumx,sumy,sumz);
	return sqrt(sq(CentreErrors[0]) + sq(CentreErrors[1]) + sq(CentreErrors[2]));
}
