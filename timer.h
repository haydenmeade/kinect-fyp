/*
 * timer.h
 *
 *  Created on: 21 Apr 2016
 *      Author: hayden
 */

#ifndef TIMER_H_
#define TIMER_H_

class timer {
	public:
		timer();
		void           start();
		void           stop();
		void           reset();
		bool           isRunning();
		unsigned long  getTime();
		bool           isOver(unsigned long seconds);
	private:
		bool           resetted;
		bool           running;
		unsigned long  beg;
		unsigned long  end;
};

#endif /* TIMER_H_ */
