/*
 * keyFrameVector.h
 *
 *  Created on: 15 Sep 2016
 *      Author: hayden
 */

#ifndef KEYFRAMEVECTOR_H_
#define KEYFRAMEVECTOR_H_

#include <vector>
#include "keyFrame.h"

using namespace std;
using namespace TooN;

class keyFrameVector{
public:
	keyFrameVector(int n); // To initialise vector
	void reset(int n); //reset vector
	keyFrame* search(int * num2,SE3<> *Eicp); // searches for transformation Eicp between keyframe num2 and live frame, returns pointer to this keyframe and Eicp num2->live frame
	void NeighbourScan(vector<float> *handCurrent, vector<float> *ICPErrors); // scans neighbours of current key frame
	void VectorSearch(vector<float> *handCurrent, vector<float> *ICPErrors, const float threshold); // scans graph (excluding current key frame and neighbours
	float ICP(vector<float> *handCurrent, int keyFramePos); //perform ICP on handcurrent, on keyframe n, returns error

	int currentKeyFrame; // current keyframe being tracked
	vector<keyFrame> KeyFrames; // vector of keyframes
};

#endif /* KEYFRAMEVECTOR_H_ */
