#include <cstdlib>
#include <iostream>
#include <cublas_v2.h>

using namespace std;

void gpu_blas_mmul(const float *A, const float *B, float *C, const int m, const int k, const int n) {
     int lda=m,ldb=k,ldc=m;
     const float alf = 1;
     const float bet = 0;
     const float *alpha = &alf;
     const float *beta = &bet;

     // Create a handle for CUBLAS
     cublasHandle_t handle;
     cublasCreate(&handle);

     // Do the actual multiplication
     cublasSgemm(handle, CUBLAS_OP_N, CUBLAS_OP_N, m, n, k, alpha, A, lda, B, ldb, beta, C, ldc);

     // Destroy the handle
     cublasDestroy(handle);
}

//Print matrix A(nr_rows_A, nr_cols_A) storage in column-major format
void print_matrix(const float *A, int nr_rows_A, int nr_cols_A) {
	for(int i = 0; i < nr_rows_A; ++i){
		for(int j = 0; j < nr_cols_A; ++j){
			cerr << A[j * nr_rows_A + i] << " ";
		}
		cerr << std::endl;
	}
	cerr << std::endl;
}

int mmult2(float *h_At, float *h_A, float *h_B, int nr_rows_A, int nr_cols_A){
	// Multiply Matrix transpose At by Matrix A to return in B
	int nr_rows_At = nr_cols_A;
	int nr_cols_At = nr_rows_A;
	int nr_rows_B,nr_cols_B;
	nr_rows_B = nr_cols_A;
	nr_cols_B = nr_cols_A;
	
	// Allocate arrays on GPU
	float *d_A, *d_At, *d_B;

	cudaMalloc(&d_At,nr_rows_At * nr_cols_At * sizeof(float));
	cudaMalloc(&d_A,nr_rows_A * nr_cols_A * sizeof(float));
	cudaMalloc(&d_B,nr_rows_B * nr_cols_B * sizeof(float));
	
	// copy arrays onto gpu
	cudaMemcpy(d_At, h_At, nr_rows_At * nr_cols_At * sizeof(float),cudaMemcpyHostToDevice);
	cudaMemcpy(d_A, h_A,   nr_rows_A * nr_cols_A * sizeof(float),cudaMemcpyHostToDevice);
	
	// matrix multiplication
	gpu_blas_mmul(d_At, d_A, d_B, nr_rows_At, nr_cols_At, nr_cols_A);
	cudaFree(d_At);cudaFree(d_A);
	// copy result
	cudaMemcpy(h_B,d_B,nr_rows_B * nr_cols_B * sizeof(float),cudaMemcpyDeviceToHost);

	//Free GPU memory
	cudaFree(d_B);
	if(cudaGetLastError()) cerr << "error: "<< cudaGetErrorName(cudaGetLastError()) << endl;
	return 1;	
}

int mmult3_2(float *h_A, float *h_Jt, float *h_E,float *h_R, int nr_rows_Jt, int nr_cols_Jt){
	// Multiply mxm by mxn by nx1 to return mx1 matrix
	// Set sizes
	int nr_rows_A = nr_rows_Jt, nr_cols_A = nr_rows_Jt;
	int	nr_cols_E = nr_cols_Jt, nr_rows_E = 1;
	int nr_cols_R = nr_rows_Jt, nr_rows_R = 1;

	int nr_rows_temp = nr_rows_A;
	int	nr_cols_temp = nr_cols_Jt;
	
	// Allocate 4 arrays on GPU
	float *d_A, *d_Jt, *d_E, *d_R, *d_temp;
	cudaMalloc(&d_A,nr_rows_A * nr_cols_A * sizeof(float));
	cudaMalloc(&d_Jt,nr_rows_Jt * nr_cols_Jt * sizeof(float));
	cudaMalloc(&d_temp,nr_rows_temp * nr_cols_temp * sizeof(float));
	cudaMemcpy(d_A,h_A,nr_rows_A * nr_cols_A * sizeof(float),cudaMemcpyHostToDevice);
	cudaMemcpy(d_Jt,h_Jt,nr_rows_Jt * nr_cols_Jt * sizeof(float),cudaMemcpyHostToDevice);
	// Multiply A and B on GPU
	gpu_blas_mmul(d_A, d_Jt, d_temp, nr_rows_A, nr_cols_A, nr_cols_Jt);
	
	// Free Memory
	cudaFree(d_A);
	cudaFree(d_Jt);
	
//	// Allocate and copy C/D
	cudaMalloc(&d_E,nr_rows_E * nr_cols_E * sizeof(float));
	cudaMalloc(&d_R,nr_rows_R * nr_cols_R * sizeof(float));
	cudaMemcpy(d_E,h_E,nr_rows_E * nr_cols_E * sizeof(float),cudaMemcpyHostToDevice);

	// Multiply temp and C on GPU
	gpu_blas_mmul(d_temp,d_E,d_R,nr_rows_temp,nr_cols_temp,nr_rows_E);
	// Copy the result on host memory


	cudaMemcpy(h_R,d_R,nr_cols_R * sizeof(float),cudaMemcpyDeviceToHost);
	//Free GPU memory
	cudaFree(d_E);
	cudaFree(d_R);
	cudaFree(d_temp);
	if(cudaGetLastError()) cerr << "error: "<< cudaGetErrorName(cudaGetLastError()) << endl;
	return 1;
}
