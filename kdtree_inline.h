/*
 * kdtree.cpp
 *
 *  Created on: 15 Sep 2016
 *      Author: hayden
 */
#ifndef KDTREE_inline_H_
#define KDTREE_inline_H_

#include <cstring>
#include <cstdlib>
#include "kdtree.h"

inline void swap(struct kd_node *x, struct kd_node *y){
	// swap two kd node values
	float tmp[3];
	memcpy(tmp,  x->pt, sizeof(tmp));
	memcpy(x->pt, y->pt, sizeof(tmp));
	memcpy(y->pt, tmp,  sizeof(tmp));
}

inline float dist(struct kd_node *a, struct kd_node *b)
{
	float t, d = 0.0;
	int dim = 3;
	while(dim--){
		t = a->pt[dim] - b->pt[dim];
		d+=t*t;
	}
	return d;
}

struct kd_node*
find_median(struct kd_node *start, struct kd_node *end, int idx)
{
	// to find median value to select next node along dimension idx
	// 0 - x, 1 - y, 2 - z
	// returns median node
	if(end <= start) return NULL;
	if(end == start + 1) return start;

	struct kd_node *p, *store, *md = start +(end - start) / 2;
	float pivot;
	while(1){
		pivot = md->pt[idx];
		swap(md, end - 1);
		for( store = p = start ; p < end ; p++ ){
			if(p->pt[idx] < pivot) {
				if( p != store )
					swap(p,store);
				store++;
			}
		}
		swap(store,end-1);
		if(store->pt[idx] == md->pt[idx])
			return md;
		if(store > md) end = store;
		else start = store;
	}
}

int visited;
void nearest( struct kd_node *root, struct kd_node *nd, int i, struct kd_node **best, float *best_dist,float *num)
{
	// nd is node to compare to
	// best is return node that is closest
	// best_dist is distance value
	if(!root) return;
	float d, dx, dx2;
	d = dist(root,nd); //ssd from root to nd
	dx = root->pt[i]  - nd->pt[i]; // to decide to go left or right
	dx2 = dx*dx;
	visited++;
	if(!*best || d < *best_dist){ // init and then compare to prev results
		*best_dist = d;
		*best = root;
	}
	if(!*best_dist) return;
	float acc = 1/* - (*num)*0.015*/; // calculate accuracy
	num++; // increment num
	if(++i >= 3) i = 0;
	nearest(dx > 0 ? root->left : root->right, nd, i, best, best_dist,num);
	if(dx2 >= (*best_dist)*acc) return; // Accuracy, as we search more and more subspaces, narrow search radius
	nearest(dx > 0 ? root->right : root->left, nd, i, best, best_dist,num);

}

struct kd_node*
make_tree (struct kd_node *t, int len, int i) {
	// To build kd tree
	// i is dim to split along (0,1,2) -> (x,y,z)
	// len is length of branch
	struct kd_node *n; //next node

	if(!len) return 0; //if at end

	if((n = find_median(t, t + len, i))) {
		// n is now a pointer to the median point along i
		i = (i + 1) % 3; // split along next dim for left/right nodes

		n->left =  make_tree(t	  ,n - t		  , i);

		n->right = make_tree(n + 1,t + len - (n+1), i);
	}
	return n;
}

#endif /* KDTREE_inline_H_ */
