/*
 * keyFrame.cpp
 *
 *  Created on: 15 Sep 2016
 *
 *     Author: hayden
 */
#include "keyFrame.h"
#include "timer.h"
#include "kdtree.h"
#include <TooN/GR_SVD.h>
#include <TooN/se3.h>
#include <TooN/SymEigen.h>

extern struct kd_node* make_tree (struct kd_node *t, int len, int i);

using namespace std;
using namespace cv;
using namespace TooN;

keyFrame::keyFrame(){
	exist = false;
	kdtree = false;
	KFconnections.clear();
	EcurrentZero();
}

keyFrame::keyFrame(int n){
	exist = kdtree = false;
	elemCount = num2[1] = num2[2] = error = 0;
	root = 0;
	KFconnections.clear();
	num2[0] = n;
	EcurrentZero();
}

keyFrame::keyFrame(int n[],keyFrame &connectTo){
	exist = kdtree = false;
	for(int i = 0; i < 3; i++) num2[i] = n[i];
	KFconnections.clear();
	KFconnections.reserve(1);
	KFconnections.push_back(keyFrameConnection(&connectTo,connectTo.Ecurrent.inverse())); // TODO is this E right?
	EcurrentZero();
}

void keyFrame::reset(int currentElemCount,std::vector<float>handCurrent, std::vector<std::vector<cv::Point> >contoursInput){
	exist = true;
	hand.clear();
	hand.reserve(currentElemCount*3);
	hand = handCurrent;
	error = 0;
	elemCount = currentElemCount;
	contours = contoursInput;
	kdtree = true;
	constructKDTree();
	EcurrentZero();
}

void keyFrame::constructKDTree(){
// construct KD tree of root node
	struct kd_node *pointsRoot;
	pointsRoot = (struct kd_node*) calloc(elemCount, sizeof(struct kd_node)); // Alloc mem for kd tree for rootKF

	for(int i = 0; i < elemCount; i++){
		pointsRoot[i].idx = i;
		pointsRoot[i].pt[0] = hand[i*3];
		pointsRoot[i].pt[1] = hand[i*3+1];
		pointsRoot[i].pt[2] = hand[i*3+2];
	}

	root = make_tree(pointsRoot, elemCount, 0); //make kd tree
}

void keyFrame::EcurrentZero(){
	Ecurrent = SE3<>::exp(makeVector(0,0,0,0,0,0));
}
