/*
 * axisCords.h
 *
 *  Created on: 15 Sep 2016
 *      Author: hayden
 */

#ifndef AXISCORDS_H_
#define AXISCORDS_H_

#include <vector>
#include "keyFrame.h"
#include <TooN/se3.h>
#include "keyFrameVector.h"

#define DEPTHROWS 424
#define DEPTHCOLS 512 // numElems = ~217k

extern void xyztoRC(float x,float y,float z, int& r, int& c);

using namespace TooN;
using namespace std;

class axisCords{
public:
	axisCords(); //constructor
	void clear(); // to clear for reset
	void fillInitialAxis(); // initialise with data
	void changeAxisSize(int frameNum); // change size of axis
	void initialiseAxis(vector<float> *handCurrent); // init with hand points
	SE3<> getEV(keyFrameVector KFV); // gets transformation between current keyframe and root
	void getCurrentCords(); // fills camera coordinates
	float getCentreErrorV(keyFrameVector KFV, vector<float> *handCurrent); // gets error of centrepoint, and centre of hand mass

	vector<float> initialWorldCoordinates; // Initial axis coordinate frame
	SE3<> E; // Estimated Transform between current axis -> initial axis
	vector<float> currentWorldCoordinates; // Current axis coordinate frame
	vector<int> currentCamCoordinates; // Current Coordinates in camera coordinate frame (to draw)
	vector<float> axisData; // Initial axis points with no axis
	vector<float> currentCentrePoint; // self explanatory
	float axisLength; // length of axis
};

#endif /* AXISCORDS_H_ */
