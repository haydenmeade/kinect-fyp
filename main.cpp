
/*
 * HAND GESTURE RECOGNITION USING THE KINECT
 * By Hayden Meade
 */

////
// INCLUDES

#include <iostream>
#include <cstdlib>
#include <signal.h>
#include <math.h>
#include <fstream>
#include <sstream>

#include <libfreenect2/libfreenect2.hpp>
#include <libfreenect2/frame_listener_impl.h>
#include <libfreenect2/packet_pipeline.h>
#include <libfreenect2/logger.h>
#include <libfreenect2/registration.h>

#include <opencv2/core.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/highgui.hpp>

#include <TooN/TooN.h>
#include <TooN/GR_SVD.h>
#include <TooN/se3.h>
#include <TooN/SymEigen.h>

#include "timer.h" //timer class
#include "kdtree.h"
#include "kdtree_inline.h"
#include "keyFrame.h" //keyframe header
#include "keyFrameVector.h"
#include "axisCords.h"

////
// DEFINES
#define DEPTHROWS 424
#define DEPTHCOLS 512 // numElems = ~217k
#define RGBROWS 1080
#define RGBCOLS 1920
#define DISPLAYENABLED true
#define sq(in) in*in

////
// NAMESPACES
using namespace std;
using namespace cv;
using namespace TooN;
using namespace libfreenect2;

///GLOBAL VARIABLES
bool shutdown = false; // if set to true loop will exit on next iteration
bool pause = false; // will pause loop after display function if set to true and wait for keypress
Registration* registration; // Global since I can't make it work otherwise
Frame undistorted(DEPTHCOLS,DEPTHROWS,4); //undistorted depth image
Freenect2Device::IrCameraParams depthParams; // Depth camera parameters
Freenect2Device::ColorCameraParams colourParams; //Colour camera parameters

////////////////////
// Objects

struct n_component{
	int n;
	vector<int> indexes;
};

////
// FUNCTION DECLARATIONS

void xyztoRC(float x,float y,float z, int& r, int& c){
	const float bad_point = numeric_limits<float>::quiet_NaN();
	const float cx(depthParams.cx), cy(depthParams.cy);
	const float fx(1/depthParams.fx), fy(1/depthParams.fy);
	if (isnan(z) || z <= 0.001)
		c = r = bad_point;
	else
	{
		c = x / (z * fx) + cx - 0.5;
		r = y / (z * fy) + cy - 0.5;
	}
}

void zeroArray(float * Array, int numElems){
	// Zeros floating point array with set number of elems
	for(int i = 0; i < numElems; i++)
		Array[i] = 0.f;
}

void threshold(float * depthData,int numElems,int minThresh,int maxThresh){
	for(int i=0;i<numElems;i++){
		if(depthData[i]>maxThresh || depthData[i] < minThresh)
			depthData[i] = 0;
	}
}

void displayDepth(Mat depthIn, const float minD,const float maxD){
	namedWindow("Display window");
	Mat copyTemp;
	depthIn.convertTo(copyTemp,CV_8UC1,255.0/(maxD-minD),-255.0/minD); // convert to grayscale image
	imshow("Display window",copyTemp);
	waitKey(1);
}

inline void rctoXYZ(int r, int c, float& x, float& y, float& z){
	const float bad_point = numeric_limits<float>::quiet_NaN();
	const float cx(depthParams.cx), cy(depthParams.cy);
	const float fx(1/depthParams.fx), fy(1/depthParams.fy);
	float* undistorted_data = (float *)undistorted.data;
	const float depth_val = undistorted_data[512*r+c]/1000.0f; //scaling factor, so that value of 1 is one meter.
	if (isnan(depth_val) || depth_val <= 0.001)
		x = y = z = bad_point;//depth value is not valid
	else
	{
		x = (c + 0.5 - cx) * fx * depth_val;
		y = (r + 0.5 - cy) * fy * depth_val;
		z = depth_val;
	}
}

inline
void fillPointCloud(const vector <int>& processed, vector<float>& handCurrent){
	handCurrent.clear();
	handCurrent.reserve(processed.size()*3);
	for(int i = 0; i < processed.size(); i++){
		float x,y,z;
		int row = processed.at(i) / DEPTHCOLS; //row of current pixel that has been processed
		int col = processed.at(i) - row * DEPTHCOLS; //col
		rctoXYZ((int)row,(int)col,x,y,z);
		if(isnan(x) || isnan(y) || isnan(z)){
			cerr << "Bad Point in Point Cloud" << endl;
			continue;
		}
		handCurrent.push_back(x);
		handCurrent.push_back(y);
		handCurrent.push_back(z);
	}
}

inline bool FloodFill(const int minDist, //min dist from kinect
		Mat& depthCurrent, //output
		vector<int>& processed, //output
		vector<vector<Point> >& contours // to draw hand outline
){
	const int numElems = DEPTHROWS * DEPTHCOLS;
	Mat closestPointMask = Mat(DEPTHROWS,DEPTHCOLS,CV_8UC1,Scalar(0)); //mask
	Mat closestPointMaskOUT = Mat(DEPTHROWS,DEPTHCOLS,CV_8UC1,Scalar(0));
	Mat copyTemp = Mat(DEPTHROWS,DEPTHCOLS,CV_32F,Scalar(0));
	unsigned int closestIdx = 0; //idx of closest point to kinect
	unsigned int idx = 0,r,c,i,j;
	unsigned int iterations = 0; // number of iterations in flood fill
	unsigned int min_component_size = 700; // min number of pixels for component to be considered a hand
	bool handFound = true; //output variable
	vector<int> queue; // queue for flood fill
	queue.clear();
	queue.reserve(numElems);
	processed.clear();
	processed.reserve(numElems);
	unsigned int componenti = 0;
	unsigned int ffidx = 0;
	vector<n_component> components;
	components.push_back(n_component());

	repeatff:
	queue.push_back(ffidx);
	while(!queue.empty()){
		iterations++;
		idx = queue.front(); //use first entry of queue for idx
		queue.erase (queue.begin()); // remove first entry of queue

		if(idx < 0 || idx > numElems) continue; //skip invalid

		r = idx / DEPTHCOLS; //row
		c = idx - r * DEPTHCOLS; //col
		float currentPointValue = depthCurrent.at<float>(r,c);

		if(((int)closestPointMask.at<unsigned char>(r,c) > 0) || currentPointValue < minDist)// check if processed or if Value is too close
			continue;
		else if(queue.size() == 0){
			components.push_back(n_component());
			components[componenti].n = ++componenti;
		}

		// if point is attached then we want to set mask at idx to 255
		// and add north,east,south,west points to the queue
		closestPointMask.at<unsigned char>(r,c) = (unsigned char)(componenti+1);
		processed.push_back(idx); //add current pixel to pixels already processed
		components[componenti].indexes.push_back(idx); //add each pixel idx onto vector in this component
		// add neighbours to queue
		queue.push_back(idx-DEPTHCOLS); //north
		queue.push_back(idx+1); //east
		queue.push_back(idx+DEPTHCOLS); //south
		queue.push_back(idx-1); //west
		queue.push_back(idx-DEPTHCOLS+1); //north east
		queue.push_back(idx+DEPTHCOLS+1); //south east
		queue.push_back(idx+DEPTHCOLS-1); //south west
		queue.push_back(idx-DEPTHCOLS-1); //north west

	}
	if(ffidx < numElems) {
		ffidx++;
		goto repeatff;
	}

	i = 0;
	while(1){ // We want to find the one component that satisfies our conditions
		if(i >= components.size()) break;
		if(components[i].indexes.size() < min_component_size){ //if component has < 1000 elements remove
			components.erase(components.begin() + i);
		}
		else {
			++i;
		}
	}

	processed.clear();
	int minComponent = 0;
	if(components.size()){ //If component Found
		srand(time(NULL)); //for random average
		vector<float> componentAverage(components.size(),0.0); //vector of component averages
		int numToAverage = 100; //number of points to average
		for(i = 0; i < components.size() ; i++){
			for(j = 0; j < numToAverage; j++){
				//pick n random pixels from the components left, take the average depth value
				idx = components[i].indexes[(int)(rand() % components[i].indexes.size())]; // random idx
				r = idx / DEPTHCOLS; //row
				c = idx - r * DEPTHCOLS; //col
				componentAverage[i] += depthCurrent.at<float>(r,c);
			}
			componentAverage[i] /= numToAverage;
		}

		float minVal = 99999.9f;
		for(i = 0 ; i < components.size() ; ++i){
			if(componentAverage[i] < minVal && componentAverage[i] > minDist){
				minVal = componentAverage[i];
				minComponent = i;
			}
		}

		for(i = 0 ; i < components[minComponent].indexes.size() ; ++i){ // fill mask
			idx = components[minComponent].indexes[i]; // idx
			r = idx / DEPTHCOLS; //row
			c = idx - r * DEPTHCOLS; //col
			if(r > DEPTHROWS || c > DEPTHCOLS){ cerr << "what" <<endl; break;}
			closestPointMaskOUT.at<unsigned char>(r,c) = (unsigned char)(255);
		}
		processed.reserve(components[minComponent].indexes.size());
		processed = components[minComponent].indexes;
	}
	else{
		handFound = false;
		cerr << "No Hand" << endl;
	}
	findContours(closestPointMaskOUT,contours,RETR_EXTERNAL, CHAIN_APPROX_NONE); // find contours to draw on image
	depthCurrent.copyTo(copyTemp,closestPointMaskOUT); //apply mask, output is: depthCurrent
	copyTemp.copyTo(depthCurrent);
	return handFound;
}

void minVector(vector<float>inVector,int *minIdx,float *minVal){
	int n = inVector.size();
	if(n < 1) return;
	*minIdx = 0;
	*minVal = inVector[*minIdx];
	for(int i = 0;i < n; i++){
		if(*minVal > inVector[i]){
			*minIdx = i;
			*minVal = inVector[i];
		}
	}
}

void waitFor (unsigned long usecs) {
    timer timer;
    timer.start();
    int i = 0;
    while (timer.getTime() < usecs){
    	++i;
    }               // Loop until it arrives.
}

void printfVector(vector<float> *input,bool space){
	/* prints an error floating point vector, so ignores values of 999*/
	stringstream ss;
	for(int i = 0; i < input->size(); i++)
		if(input->at(i) < 999){
			ss << i << ": ";
			if(space)
				ss << input->at(i) << " ";
			else
				ss << input->at(i) << endl;
		}
	cout << ss.str() << endl;
}

void subSample(vector<float> *handCurrent, vector<float> *handCurrentSubSmpl, int subSampFreq){
	int currentElemCount = handCurrent->size() / 3;
	handCurrentSubSmpl->clear();
	handCurrentSubSmpl->reserve(currentElemCount / subSampFreq * 3);
	for(int i = 0; i < currentElemCount; i++){
		if(i % subSampFreq == 0){
			handCurrentSubSmpl->push_back(handCurrent->at(i*3));// fill subsampled hand
			handCurrentSubSmpl->push_back(handCurrent->at(i*3+1));
			handCurrentSubSmpl->push_back(handCurrent->at(i*3+2));
		}
	}
}

void subSampleContours(vector<float> *handCurrentSubSmpl,vector<vector<Point> >& contours ){
	int contourPointCount = 0;
	int i,j;
	for(i = 0; i < contours.size(); ++i){
		contourPointCount+=contours[i].size();
	}
	// want to subsample points along contours
	handCurrentSubSmpl->clear();
	handCurrentSubSmpl->reserve(contourPointCount * 3);
	cerr<< "1: " << contourPointCount << endl;
	contourPointCount = 0;
	for(i = 0; i < contours.size(); ++i){
		for(j = 0; j < contours[i].size(); ++j){
			float x,y,z;
			rctoXYZ((int)contours[i][j].x,(int)contours[i][j].y,x,y,z);
			if(isnan(x) || isnan(y) || isnan(z)){
				//cerr << "Bad Point " << endl;
				continue;
			}
			++contourPointCount;
			handCurrentSubSmpl->push_back(x);// fill subsampled hand
			handCurrentSubSmpl->push_back(y);
			handCurrentSubSmpl->push_back(z);
		}
	}
	cerr << contourPointCount << endl;

}


int startKinect(Freenect2 *freenect2, Freenect2Device *dev, SyncMultiFrameListener *listener){
	PacketPipeline *pipeline = 0;
	string serial = "";
	if(freenect2->enumerateDevices() == 0){
		cout << "no device connected!" << endl;
		return -1;
	}
    serial = freenect2->getDefaultDeviceSerialNumber();
    //pipeline
	pipeline = new CudaPacketPipeline();

	//open
	dev = freenect2->openDevice(serial,pipeline);

	// listeners
	dev->setColorFrameListener(listener);
	dev->setIrAndDepthFrameListener(listener);

	//start
    if (!dev->start()) return -1;
	cout << "device serial: " << dev->getSerialNumber() << endl;
	cout << "device firmware: " << dev->getFirmwareVersion() << endl;

	///// registration setup
	depthParams = dev->getIrCameraParams();
	colourParams = dev->getColorCameraParams();
	registration = new Registration(depthParams,colourParams);
    return 0;
}

int main(int argc, char* argv[])
{
	const int numElems = DEPTHROWS*DEPTHCOLS;
	const bool enable_rgb = true;
	const bool enable_depth = true;
	const size_t framemax = -1;
	const int timeout = 1000;
	timer timer,looptimer;
	float FPS = 0;
	size_t framecount = 0; // frame number

	///////////////////////
	// Initialise and start kinect
	Freenect2Device *dev = 0; // Kinect device
	int types = Frame::Depth | Frame::Color; //Types of frames to listen for
	FrameMap frames; // Frames
    SyncMultiFrameListener listener(types); // Frame listener
    Freenect2 freenect2; // freenect 2 object
    if(startKinect(&freenect2, dev, &listener) != 0) {
        cerr << "Kinect Error" << endl;
        return -1;
    }
	Frame registered(DEPTHCOLS,DEPTHROWS,4); // Registered colour depth image

	/// File input or output for testing
	const int inputFrames = 1; // input frames from file-
	const int outputFrames = 0; // output frames from file
	FILE *ostream = fopen("./Data/testingDataOut.data","wb");
	ifstream ifs("./Data/testingDataIn.data",ios::binary);
//	FILE *ostream3 = fopen("./Data_small/Nathan/DepthData","wb");FILE *ostream5 = fopen("./Data_small/Nathan/colour_depth_map_b","wb");

	// Saving Video and console to file
	Size FrameSize(DEPTHCOLS,DEPTHROWS);
	stringstream outname,ss;
	outname << "./Data/Test9/Output_N" << 1;
	FILE *consoleText = freopen(outname.str().c_str(),"w",stdout);
	outname << ".avi";
//	VideoWriter OVideoWriter(outname.str(),CV_FOURCC('m','p','4','v'),30,FrameSize,true);
//	if(!OVideoWriter.isOpened()){
//		cerr << "No Video" << endl;
//		return -1;
//	}

	/// Thresholds
	const unsigned int numberOfKeyFrames = 50; //number of keyframes to use

	const float switchingKeyFrameThreshold = 0.9; // threshold to switch to next keyframe if at the end of the line
	//const float errorMeasurementSwitchDifferenceThreshold = 0.5;// diff threshold between lowest + current keyframe
	const float upperET = 0; // create new keyframe
	const float resetErrorThreshold = 200;
	// TODO
	float ICPErrorToCreateKeyFrame = 0.0027; // create new keyframe
	float ICPErrorToScan = 0.0021; // error to scan neighbours
	float ICPerrorToSearch = 0.0026; // error to search graph for lowest error
	const float ICPErrorToReset = 0.011; // error to reset
	unsigned int subSampFreq = 1; // take every nth point
	const unsigned int maxSubSampFreq = 10; // max sub samp rate
	const int minThresh = 250; // Thresholds for depth image (in mm)
	const int maxThresh = 1000;
	const Scalar minYCrCb (50,133,80); // min/max skin colours
	const Scalar maxYCrCb (255,173,120);
	const Scalar blue = Scalar(255,0,0); // colour variables
	const Scalar red = Scalar(0,0,255);
	const Scalar green = Scalar(0,255,0);

	stringstream stats;
	std::ofstream ostream2;
	ostream2.open("./Data_small/OutputStats", ios::app);
	int testnum = 1;
	//for(ICPErrorToCreateKeyFrame = 0.0027; ICPErrorToCreateKeyFrame < 0.0033; ICPErrorToCreateKeyFrame += 0.0001){
	//for(ICPerrorToSearch = 0.0025; ICPerrorToSearch < 0.0029; ICPerrorToSearch += 0.0001)
	//for(ICPErrorToScan = 0.0018; ICPErrorToScan < 0.0024; ICPErrorToScan += 0.0001){
	//if(shutdown || ICPErrorToCreateKeyFrame < ICPerrorToSearch ) continue;

	
	Mat depthImage; // raw depth image (not filtered)
	Mat depthRGB; /* Registered image, depth image with RGB colours */
	Mat bgrImage; /* RGB image 1920x1080 */
	Mat imageYCrCb; /* Colour image in Y Cr Cb to detect skin */
	Mat depthCurrent; /* Current depth Image (fully processed) */
	Mat skinRegion; /* Mask for skin region */
	Mat copyTemp;/*temp matrix for copying */

	int i,j; // counters
	int *colour_depth_map = (int *)malloc(DEPTHROWS * DEPTHCOLS * sizeof(int)); //colour map for registration
	unsigned char *depthChars[numElems]; // Array for depth image data
	vector<float> handCurrent; // point cloud of current found hand (x1,y1,z1,x2,y2,z2....,xn,yn,zn)
	vector<int> cameraPoints; // Points for drawing axis on depth image


	keyFrameVector KFV(numberOfKeyFrames); // key frame vector object

	axisCords axisCords; // Coordinate frame object

	//Various timer variables used to profile
	timer.start();
	looptimer.start();
	unsigned long looptime = 0; // to store time taken to complete loop
	unsigned long GetTimeFrame = 0,GetTimeDepthThreshold = 0,GetTimeSkinThreshold = 0,GetTimeFloodFill = 0,
			GetTimeHandFill = 0,GetTimeICP = 0,GetTimeDisplay = 0,GetTimeAxis = 0;

	float AverageICPError = 0, AverageAxisError = 0; //stats

	waitFor(1000000);
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Loop start////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	cerr << "Starting . . . "<< endl;
	while(!shutdown && (framemax == (size_t)-1 || framecount < framemax))
	{
		++framecount;
		cout << endl << "Frame: " << framecount << endl;
		int success = listener.waitForNewFrame(frames,timeout);
		looptimer.reset(); timer.reset();
		if(!success) {
			cerr << "Failed to grab frame." << endl;
			continue;
		}

		// grab frames
		Frame *rgb = frames[Frame::Color];
		Frame *depth = frames[Frame::Depth];
		if(inputFrames){
			ifs.read((char *)(rgb->data), RGBCOLS*RGBROWS*(rgb->bytes_per_pixel));
			ifs.read((char *)(depth->data), numElems*(depth->bytes_per_pixel));
			if(ifs.eof()){
				cout << "End of file reached." << endl;
				ifs.clear(); // Reset and start at beginning of file, for testing multiple constants
				ifs.seekg(0,ios::beg);
				goto exit;
			}

		} else if(outputFrames){
			fwrite((char *)rgb->data,1,RGBCOLS*RGBROWS*(rgb->bytes_per_pixel),ostream);
			fwrite((char *)(depth->data),1,numElems*(depth->bytes_per_pixel),ostream);
		}

		// apply built in registration function to overlay colour on depth image
		registration->apply(rgb,depth,&undistorted,&registered,true,NULL,colour_depth_map);

		depthRGB = Mat(DEPTHROWS,DEPTHCOLS,CV_8UC4,registered.data); // overlay rgb on depth
		bgrImage = Mat(RGBROWS,RGBCOLS,CV_8UC4,rgb->data); //rgb image from kinect
		depthImage = Mat(DEPTHROWS,DEPTHCOLS,CV_32F,depth->data);
		listener.release(frames); // release frames
		GetTimeFrame = timer.getTime();timer.reset();

		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// DEPTH IMAGE THRESHOLD
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// want to threshold between minThresh and maxThresh (in mm)
		memcpy(depthChars,depth->data,numElems*(depth->bytes_per_pixel)); // copy depth data for processing
		float * depthData = (float*)depthChars; //float depth image values
		threshold(depthData,numElems,minThresh,maxThresh); // call threshold func
		depthCurrent = Mat(DEPTHROWS,DEPTHCOLS,CV_32F,(unsigned char *)depthData); // create image from depth data (thresholded)

		GetTimeDepthThreshold = timer.getTime(); timer.reset();

		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// RGB IMAGE SKIN SEGMENTATION
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// Inputs: Min/Max skin colours in YCrCb, depthRGB image
		// Outputs: vector of contours to draw, with index: hierarchy, and the Skin Image Mask
		// Reset Mats to zero
		skinRegion = Mat(DEPTHROWS,DEPTHCOLS,CV_8U,Scalar(0,0,0)); // mask of skin pixels

		cvtColor(depthRGB,imageYCrCb,CV_BGR2YCrCb); //use ycrcb since paper said it's better
		inRange(imageYCrCb,minYCrCb,maxYCrCb,skinRegion); // filter reg image between the skin colours

		// apply skin mask to depth image
		copyTemp = Mat(DEPTHROWS,DEPTHCOLS,CV_32F,Scalar(0,0,0)); // Init copyTemp to 0
		depthCurrent.copyTo(copyTemp,skinRegion);
		copyTemp.copyTo(depthCurrent);
		GetTimeSkinThreshold = timer.getTime();timer.reset();

		/////////////////////////////////////////////////////////////////////////////////////////
		// Perform Flood Fill on closest pixels to kinect (that are skin)
		/////////////////////////////////////////////////////////////////////////////////////////////
		vector<int> processed; // to keep track of pixels already processed (indexes)
		bool handFound;
		vector<vector<Point> > contours; // for use in find contours (for drawing on image)

		handFound = FloodFill(minThresh, depthCurrent, processed, contours);
		if(processed.size() > MAXPOINTSICP) {
			cerr << "Processed size: " << processed.size() << endl << "Processed vector Error" << endl;
			handFound = 0;
			continue;
		}
		GetTimeFloodFill = timer.getTime(); timer.reset();

		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		///	fill pointcloud ////////////////////////////////////////////////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////
		bool skipICP = false;
		int currentElemCount = processed.size(); // Amount of points in point cloud

		if(handFound){
			fillPointCloud(processed,handCurrent);
			if(KFV.currentKeyFrame == -1){
				// Initialise Root Hand if it hasn't been already or if reset
				KFV.currentKeyFrame = 0;
				KFV.KeyFrames.push_back(keyFrame(0));
				KFV.KeyFrames[KFV.currentKeyFrame].reset(currentElemCount,handCurrent,contours);// reinitialise current key frame
				if(KFV.currentKeyFrame == 0) axisCords.initialiseAxis(&handCurrent); // initialise axis points
				skipICP = true;
			}
		} else skipICP = true; // if no hand was found, skip ICP

		GetTimeHandFill = timer.getTime(); timer.reset();

		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//// 	ICP ////////////////////////////////////////////////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		vector <float> ICPerrors(numberOfKeyFrames, 999.0); // vector of errors of icp at each key frame
		vector <float> AxisErrors(numberOfKeyFrames, 999.0); // vector of errors of centre point of axis error
		if(outputFrames||(framecount == 1)) skipICP = true;
		if(!skipICP){ // Assuming both handFound and handFoundPrev are filled
			const int maxIterations = 25;
			const float precision = 1e-4;
			////////////////////////////////////
			////// sub-sampling

			if(FPS < 26 && subSampFreq < maxSubSampFreq) subSampFreq++;
			else if(subSampFreq > 1) subSampFreq--;
			vector<float> handCurrentSubSmpl; // Subsampled hand point cloud
//			cerr << subSampFreq << endl;
			subSample(&handCurrent, &handCurrentSubSmpl, subSampFreq);
//			subSampleContours(&handCurrentSubSmpl,contours);

			/////////////////////////////////
			// Performing ICP
			// ICP on current frame
			ICPerrors[KFV.currentKeyFrame] = KFV.ICP(&handCurrentSubSmpl, KFV.currentKeyFrame); // icp on current kf

			int minerri = KFV.currentKeyFrame; // index of min error
			float minerr = ICPerrors[KFV.currentKeyFrame]; // min error value

			//if error greater than a threshold then scan neighbours
				//then:
			if(minerr > ICPErrorToScan){
				subSample(&handCurrent, &handCurrentSubSmpl, subSampFreq);
//				subSampleContours(&handCurrentSubSmpl,contours);

				KFV.NeighbourScan(&handCurrentSubSmpl, &ICPerrors);
				minVector(ICPerrors,&minerri,&minerr);
			}
			if(minerr > ICPerrorToSearch){
				subSample(&handCurrent, &handCurrentSubSmpl, maxSubSampFreq);
//				subSampleContours(&handCurrentSubSmpl,contours);

				cout << "Searching " << endl;
				ICPerrors[KFV.currentKeyFrame] = 999.f;
				KFV.VectorSearch(&handCurrentSubSmpl, &ICPerrors, ICPerrorToSearch);
				minVector(ICPerrors, &minerri, &minerr);
				//printfVector(&ICPerrors,true);
				if(minerri != KFV.currentKeyFrame) cout << "Switching to " << minerri<< endl;
			}
			//cerr << KFV.KeyFrames.at(KFV.currentKeyFrame).KFconnections.size() << endl;
			printfVector(&ICPerrors,true);
			// switch to lowest error keyframe
			if(minerr < ICPErrorToCreateKeyFrame)
				KFV.currentKeyFrame = minerri;

			// if still greater than some other thresholdthen create new keyframe
			// TODO otherwise if greater than allowable, do not create new keyframe, instead scan through graph, then create new frame from lowest error or switch

			if(minerr > ICPErrorToCreateKeyFrame && KFV.KeyFrames.size() < numberOfKeyFrames){
				// create frame
				int numarray[] = {KFV.KeyFrames.size(), KFV.currentKeyFrame, KFV.KeyFrames.at(KFV.currentKeyFrame).KFconnections.size()}; // number of keyframe, parent connection, connection number
				KFV.KeyFrames.push_back(keyFrame(numarray,KFV.KeyFrames[KFV.currentKeyFrame])); // add new keyframe
				KFV.KeyFrames[KFV.currentKeyFrame].KFconnections.push_back(keyFrameConnection(&(KFV.KeyFrames[numarray[0]]),KFV.KeyFrames[KFV.currentKeyFrame].Ecurrent)); // add connection to current keyframe
				KFV.currentKeyFrame = numarray[0]; // change current keyframe
				KFV.KeyFrames[KFV.currentKeyFrame].reset(currentElemCount,handCurrent,contours); // fill new keyframe
				AxisErrors[KFV.currentKeyFrame] = 0;
			}else if(minerr > ICPErrorToReset || minerr == 0){
				ifs.clear();
				ifs.seekg(0,ios::beg);
				goto exit;
				skipICP = true;
				axisCords.clear();
				KFV.reset(numberOfKeyFrames); // reset key frames
				cerr << "KeyFrame reset" << endl;cout << "Keyframe reset" << endl;
			}
			//cerr << "number of frames: " << KFV.KeyFrames.size() << endl;
			//printfVector(&ICPerrors,true);
			GetTimeICP = timer.getTime();
			timer.reset();

			////////////////////////////////////
			// Getting Coordinate axis points
			if(!skipICP){
				//axisCords.changeAxisSize();
				axisCords.E = axisCords.getEV(KFV);
				axisCords.getCurrentCords();
				// get axis errors:
				for(i = 0; i < numberOfKeyFrames; i++){
					if(ICPerrors[i] < 998){
						AxisErrors[i] = axisCords.getCentreErrorV(KFV,&handCurrent);
					}
				}
			}

			GetTimeAxis = timer.getTime();
			timer.reset();
		}
		else cout << "ICP SKIP" << endl;

		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// Display outputs ////////////////////////////////////////////////////////////////////////////////////////////////////////////
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		if(DISPLAYENABLED)
		{
			if(handFound)// Don't want to display if hand not found (update image feed)
			{
				// Outline Hand in display
				drawContours(depthRGB,contours,-1,green); // draw skin regions on reg image
				if(!skipICP){
					drawContours(depthRGB,KFV.KeyFrames.at(KFV.currentKeyFrame).contours,-1,red); // draw root hand
					cameraPoints = axisCords.currentCamCoordinates;
					if(cameraPoints.size() == 8)
					{
						Point
						pt1 = Point(cameraPoints[1], cameraPoints[0]), // origin
						pt2 = Point(cameraPoints[3], cameraPoints[2]),
						pt3 = Point(cameraPoints[5], cameraPoints[4]),
						pt4 = Point(cameraPoints[7], cameraPoints[6]);
						arrowedLine(depthRGB,pt1,pt2,green,1,CV_AA,0,0.1); //x
						arrowedLine(depthRGB,pt1,pt3,red,1,CV_AA,0,0.1); //y
						arrowedLine(depthRGB,pt1,pt4,blue,1,CV_AA,0,0.1); //z
					}

					// writing text on video
					ss.str(std::string());
					ss << "Node " << KFV.KeyFrames[KFV.currentKeyFrame].num2[0] << " Parent: " << KFV.KeyFrames[KFV.currentKeyFrame].num2[1] << " ConNum: " << KFV.KeyFrames[KFV.currentKeyFrame].num2[2];
					putText(depthRGB,ss.str(),cvPoint(20,125),FONT_HERSHEY_COMPLEX_SMALL,0.8,green,1,8);
				}
				ss.str(std::string());
				ss << "Test: " << testnum;
				putText(depthRGB,ss.str(),cvPoint(20,95),FONT_HERSHEY_COMPLEX_SMALL,0.8,green,1,8);
				ss.str(std::string());
				ss << "FPS " << (int)(FPS+1/(looptimer.getTime()*1e-6))/2;
				putText(depthRGB,ss.str(),cvPoint(20,110),FONT_HERSHEY_COMPLEX_SMALL,0.8,green,1,8);
				ss.str(std::string());
				ss << "Frame " << framecount;
				putText(depthRGB,ss.str(),cvPoint(20,140),FONT_HERSHEY_COMPLEX_SMALL,0.8,green,1,8);
				cvtColor(depthRGB,copyTemp, CV_BGRA2BGR);
				//OVideoWriter << copyTemp;
				imshow("Display window",depthRGB);
				cout << "Loop time: " << looptimer.getTime() << endl << " FPS: " << 1/(looptimer.getTime()*1e-6);
				if(waitKey(1) == 1048603){ // press esc to exit
					shutdown = true;
				}
				else if(waitKey(1) == 1048608){ // spacebar
					KFV.reset(numberOfKeyFrames); // reset key frames
					axisCords.clear();
					cerr << "Reset to Root Key Frame - SPACEBAR" << endl;
				}
			}
		}
		GetTimeDisplay += timer.getTime();
		timer.reset();
		looptime = looptimer.getTime(); //get time to complete loop
		AverageICPError += KFV.KeyFrames[KFV.currentKeyFrame].error;
		AverageAxisError += AxisErrors[KFV.currentKeyFrame];
		FPS = 1/(looptimer.getTime()*1e-6);
		//cout << "FPS: " << FPS <<" at subsample: " << subSampFreq << endl;

		if(1){
			cout << "Timing Stats:" << endl;
			cout << "GetFrame: " << (float)GetTimeFrame / looptime * 100 << " %" << endl;
			cout << "DepthThresh: " << (float)GetTimeDepthThreshold / looptime * 100 << " %" << endl;
			cout << "SkinThresh: " << (float)GetTimeSkinThreshold / looptime * 100 << " %" << endl;
			cout << "FloodFill: " << (float)GetTimeFloodFill / looptime * 100 << " %" << endl;
			cout << "HandFill: " << (float)GetTimeHandFill / looptime * 100 << " %" << endl;
			cout << "ICP: " << (float)GetTimeICP / looptime * 100 << " % of loop" << endl;
			cout << "Axis: " << (float)GetTimeAxis / looptime * 100 << " %" << endl;
			cout << "Display: " << (float)GetTimeDisplay / looptime * 100 << " %" << endl;
		}

		if(pause){
			int c = 0;
			cout << "Press any key + enter to continue..." << endl;
			do {
				c = getchar();
			} while (!c);
			pause = false;
		}
	}
	/// EXIT

	exit:
	for(i = 0; i < KFV.KeyFrames.size() ; i ++){
		//for each keyframe print the list of connections
		cout << "KeyFrame: " << i << endl;
		for(j = 0; j < KFV.KeyFrames[i].KFconnections.size(); j++){
			cout << "Connection to: " << KFV.KeyFrames[i].KFconnections[j].connection->num2[0] << endl;
		}
		cout << endl;
	}
	cout << endl;

	cout << "Ending at frame count: " << framecount << endl;
	cout << "Average ICP Error: " << AverageICPError/(framecount-1) << endl;
	cout << "Average Axis Error: " << AverageAxisError/(framecount-1) << endl;
	cerr << "Test: " << testnum << endl;
	cerr << "Ending..." << endl;
	stats.str(std::string());
	stats << "Test num: " << testnum << endl;
	stats <<"Create KF: "<< ICPErrorToCreateKeyFrame <<" To Search: " << ICPerrorToSearch << " To Scan: " << ICPErrorToScan << endl;
	stats << "ICP Error: " << AverageICPError/(framecount-1) << endl;
	stats << "Axis Error: " << AverageAxisError/(framecount-1) << endl;
	stats << "number of KFs: " << KFV.KeyFrames.size() << endl;
	stats << endl;
	ostream2 << stats.rdbuf();
	++testnum;
	KFV.reset(numberOfKeyFrames); // reset key frames
	axisCords.clear();
	framecount = 0;
//	} // end testing for loops
	exit2:
	/// [stop]
	ostream2.close();
	destroyAllWindows();
	fclose(stdout);
	ifs.close();
	fclose(ostream);
	listener.release(frames);
	dev->stop();
	dev->close();
	return 0;
}
