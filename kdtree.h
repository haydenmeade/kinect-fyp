/*
 * kdtree.h
 *
 *  Created on: 5 May 2016
 *      Author: rosetta code edited by hayden
 */

#ifndef KDTREE_H_
#define KDTREE_H_
struct kd_node {
	// kd node structure (3 dimensions)
	float pt[3]; // x,y,z values at current node
	int idx; //index of point in hand
	struct kd_node *left, *right; // pointers to nodes to the left/right of current
};
#endif /* KDTREE_H_ */
